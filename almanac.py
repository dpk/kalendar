import argparse
import datetime
import sys
import uuid

import kalendar


def get_observances(day):
    feast_name = None
    other_observances = None

    if day.days_after == 0:
        feast_name = day.feast
        if day.transferred:
            feast_name += ' (transferred)'

    if day.other_observances:
        other_observances = sorted(day.other_observances)

    return feast_name, other_observances


def html_almanac(year):
    yield "<table>"
    last_month = None
    
    for date, day in year:
        yield "  <tr>"
        if date.month != last_month:
            yield "    <td>%s" % date.strftime('%B')
        else:
            yield "    <td>"

        yield "    <td>%s" % date.day
        yield "    <td>%d" % day.proper_week
        yield "    <td>%s" % ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'][date.weekday()]

        date_description = ""
        feast_name, other_observances = get_observances(day)
        if feast_name:
            date_description += "<b>%s</b> " % feast_name

        if other_observances:
            descr = ', '.join('<i>%s</i>' % obs for obs in other_observances)
            date_description += '(%s)' % descr

        yield "    <td>%s" % date_description

        last_month = date.month

    yield "</table>"


def render_html(kal):
    extent = '/'.join(map(str, kal.calendar_years))
    print("<!DOCTYPE html>")
    print("<meta charset=utf-8>")
    print("<title>Almanac for the Church Year %s</title>" % extent)
    print("<h1>Almanac for the Church Year %s</h1>" % extent)
    
    print('\n'.join(html_almanac(kal)))


def render_ical(kal):
    def printi(txt):
        # ical format officially requires CRLF as line terminator, so be nice
        print("%s\r" % txt)


    printi("BEGIN:VCALENDAR")
    printi("VERSION:2.0")

    for date, day in kal:
        feast_name, other_observances = get_observances(day)
        if feast_name or other_observances:
            printi("BEGIN:VEVENT")
            uid = uuid.uuid4()
            printi("UID:%s" % uid)
            date_start = date.strftime('%Y%m%d')
            date_end = (date + datetime.timedelta(days=1)).strftime('%Y%m%d')
            # Next two lines mark this as an all-day event
            printi("DTSTART;VALUE=DATE:%s" % date_start)
            printi("DTEND;VALUE=DATE:%s" % date_end)
            summary = feast_name
            description = ', '.join(other_observances if other_observances else [])
            if not summary:
                # Only thing happening today is the other observances
                summary = '(' + description + ')'
                description = ''
            printi("SUMMARY:%s" % summary)
            printi("DESCRIPTION:%s" % description)
            printi("END:VEVENT")

    printi("END:VCALENDAR")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--ical', dest='ical', help='Output iCalendar format', action='store_true')
    parser.add_argument('year', help='Year ecclesiastical calendar ends', type=int)
    args = parser.parse_args()
    kal = kalendar.Year(args.year)
    if args.ical:
        render_ical(kal)
    else:
        render_html(kal)
