.PHONY: web clean
almanac_years = $(shell seq `(date +%Y; echo 1 - p) | dc` `(date +%Y; echo 100 + p) | dc`)

web: public/index.html $(patsubst %,public/almanac_%.html,$(almanac_years)) $(patsubst %,public/almanac_%.ics,$(almanac_years))
clean:
	rm -rf public

public:
	mkdir -p public

public/almanac_%.html: kalendar.py almanac.py | public
	python3 almanac.py $* > $@

public/almanac_%.ics: kalendar.py almanac.py | public
	python3 almanac.py --ical $* > $@


public/index.html: Makefile | public
	echo "<!DOCTYPE html><meta charset=utf-8>" > $@
	echo "<title>Church Calendar Almanacs</title>" >> $@
	echo "<h1>Church Calendar Almanacs</h1><ul>" >> $@
	$(foreach year,$(almanac_years),echo "<li>$(shell echo '$(year) 1 - p' | dc)–$(year) (<a href=\"almanac_$(year).html\">HTML</a>, <a href=\"almanac_$(year).ics\">ICS</a>)" >> $@;)
	echo "</ul>" >> $@
