import bisect
from collections import namedtuple
from datetime import date, timedelta
from functools import lru_cache
import math


### TABLES FOR THE CHRISTIAN YEAR ###
## TODO: make this some kind of modular system so that different kalendars can be loaded

last_sunday_name = "Sunday next before Advent"

advent_sundays = [
    'Advent I',
    'Advent II',
    'Advent III',
    'Advent IV',
]

# feasts only go in this list if there are Sundays which happen relative to them
fixed_feasts = [
    ('Christmas Day', (25, 12)),
    ('Epiphany', (6, 1)),
]

fixed_feast_sundays = {
    'Christmas Day': ['Christmas I', 'Christmas II'],
    'Epiphany': ['Epiphany I', 'Epiphany II', 'Epiphany III', 'Epiphany IV', 'Epiphany V', 'Epiphany VI'],
}

red_letter_days = [
    ('St Thomas, Apostle', (21, 12)),
    ('St Stephen, Martyr', (26, 12)),
    ('St John, Evangelist', (27, 12)),
    ('Innocents’ Day', (28, 12)),
    ('Circumcision', (1, 1)),
    ('Conversion of St Paul', (25, 1)),
    ('Purification of the Virgin Mary', (2, 2)),
    ('St Matthias, Apostle', (24, 2)),
    ('Annunciation of the Virgin Mary', (25, 3)),
    ('St Mark, Evangelist', (25, 4)),
    ('St Philip and St James, Apostles and Martyrs', (1, 5)),
    ('St Barnabas, Apostle', (11, 6)),
    ('St John Baptist', (24, 6)),
    ('St Peter, Apostle', (29, 6)),
    ('St Mary Magdalene', (22, 7)),
    ('St James, Apostle', (25, 7)),
    ('Transfiguration', (6, 8)),
    ('St Bartholomew, Apostle', (24, 8)),
    ('St Matthew, Apostle', (21, 9)),
    ('St Michael and All Angels', (29, 9)),
    ('St Luke, Evangelist', (18, 10)),
    ('St Simon and St Jude, Apostles', (28, 10)),
    ('All Saints’ Day', (1, 11)),
    ('St Andrew, Apostle', (30, 11)),
]

black_letter_days = [
    ('Lucian, Priest and Martyr', (8, 1)),
    ('Hilary, Bishop and Confessor', (13, 1)),
    ('Prisca, Virgin and Martyr', (18, 1)),
    ('Fabian, Bishop and Martyr', (20, 1)),
    ('Agnes, Virgin and Martyr', (21, 1)),
    ('Vincent, Deacon and Martyr', (22, 1)),
    ('Blasius, Bishop and Martyr', (3, 2)),
    ('Agatha, Virgin and Martyr', (5, 2)),
    ('Valentine, Bishop', (14, 2)),
    ('David, Archbishop', (1, 3)),
    ('Chad, Bishop', (2, 3)),
    ('Perpetua, Martyr', (7, 3)),
    ('Gregory the Great, Bishop', (12, 3)),
    ('Edward, King and Martyr', (18, 3)),
    ('Benedict, Abbot', (21, 3)),
    ('Richard, Bishop', (3, 4)),
    ('Ambrose, Bishop', (4, 4)),
    ('Alphege, Archbishop', (19, 4)),
    ('George, Martyr', (23, 4)),
    ('Invention of the Holy Cross', (3, 5)),
    ('St John Evangelist ante Portam Latinum', (6, 5)),
    ('Dunstan, Archbishop', (19, 5)),
    ('Augustine, Archbishop', (26, 5)),
    ('Venerable Bede, Priest', (27, 5)),
    ('Nicomede, Priest', (1, 6)),
    ('Boniface, Bishop', (5, 6)),
    ('Alban, Martyr', (17, 6)),
    ('Translation of King Edward', (20, 6)),
    ('Visitation of the Virgin Mary', (2, 7)),
    ('Translation of St Martin', (4, 7)),
    ('Swithun, Bishop', (15, 7)),
    ('Margaret, Virgin and Martyr', (20, 6)),
    ('St Anne', (26, 7)),
    ('Lammas Day', (1, 8)),
    ('Name of Jesus', (7, 8)),
    ('St Lawrence', (10, 8)),
    ('Augustine, Bishop', (28, 8)),
    ('Beheading of St John Baptist', (29, 8)),
    ('Giles, Abbot', (1, 9)),
    ('Enurchus', (7, 9)),
    ('Nativity of the Virgin Mary', (8, 9)),
    ('Holy Cross Day', (14, 9)),
    ('Lambert, Bishop', (17, 9)),
    ('Cyprian, Archbishop', (26, 9)),
    ('St Jerome', (30, 9)),
    ('Remigius, Bishop', (1, 10)),
    ('Faith, Virgin and Martyr', (6, 10)),
    ('St Denys, Bishop', (9, 10)),
    ('Translation of King Edward', (13, 10)),
    ('Etheldreda, Virgin', (17, 10)),
    ('Crispin, Martyr', (25, 10)),
    ('Leonard, Confessor', (6, 11)),
    ('Martin, Bishop', (11, 11)),
    ('Britius, Bishop', (13, 11)),
    ('Machutus, Bishop', (15, 11)),
    ('Hugh, Bishop', (17, 11)),
    ('Edmund, King', (20, 11)),
    ('Cecilia, Virgin and Martyr', (22, 11)),
    ('St Clement, Bishop', (23, 11)),
    ('Catherine, Virgin', (25, 11)),
    ('Nicolas, Bishop', (6, 12)),
    ('Conception of the Virgin Mary', (8, 12)),
    ('Lucy, Virgin and Martyr', (13, 12)),
    ('O Sapientia', (16, 12)),
    ('Silvester, Bishop', (31, 12)),
]

moveable_feasts = [
    ('Septuagesima',  -7 * 9),
    ('Sexagesima',    -7 * 8),
    ('Quinquagesima', -7 * 7),
    ('Ash Wednesday', (-7 * 6) - 4),
    ('Lent I',  -7 * 6),
    ('Lent II', -7 * 5),
    ('Lent III',-7 * 4),
    ('Lent IV', -7 * 3),
    ('Lent V',  -7 * 2),
    ('Sunday next before Easter', -7 * 1),
    ('Monday before Easter', -6),
    ('Tuesday before Easter', -5),
    ('Wednesday before Easter', -4),
    ('Thursday before Easter', -3),
    ('Good Friday', -2),
    ('Easter Even', -1),
    ('Easter Day', 0),
    ('Monday in Easter Week', 1),
    ('Tuesday in Easter Week', 2),
    ('Easter I',   7 * 1),
    ('Easter II',  7 * 2),
    ('Easter III', 7 * 3),
    ('Easter IV',  7 * 4),
    ('Easter V',   7 * 5),
    ('Ascension Day',  (7 * 5) + 4),
    ('Sunday after Ascension Day', 7 * 6),
    ('Whitsunday', 7 * 7),
    ('Trinity Sunday', 7 * 8),
    ('Trinity I',  7 * 9),
    ('Trinity II', 7 * 10),
    ('Trinity III',7 * 11),
    ('Trinity IV', 7 * 12),
    ('Trinity V',  7 * 13),
    ('Trinity VI', 7 * 14),
    ('Trinity VII',7 * 15),
    ('Trinity VIII',7 * 16),
    ('Trinity IX', 7 * 17),
    ('Trinity X',  7 * 18),
    ('Trinity XI', 7 * 19),
    ('Trinity XII',7 * 20),
    ('Trinity XIII',7 * 21),
    ('Trinity XIV',7 * 22),
    ('Trinity XV', 7 * 23),
    ('Trinity XVI',7 * 24),
    ('Trinity XVII',7 * 25),
    ('Trinity XVIII', 7 * 26),
    ('Trinity XIX',7 * 27),
    ('Trinity XX', 7 * 28),
    ('Trinity XXI',7 * 29),
    ('Trinity XXII',7 * 30),
    ('Trinity XXIII',7 * 31),
    ('Trinity XXIV', 7 * 32),
    ('Trinity XXV', 7 * 33),
    ('Trinity XXVI', 7 * 34),
    ('Trinity XXVII', 7 * 35),
]

transference_seasons = [
    # Ash Wednesday -> the Friday after Ash Wednesday
    ('Ash Wednesday', 0, (-7 * 6) - 2),
    # Passion Sunday -> the Tuesday after Passion Sunday
    ('Lent V', 0, (-7 * 2) + 2),
    # Palm Sunday and 14 days after (i.e. Holy Week and the Octave of Easter) -> the Tuesday after Easter I
    ('Sunday next before Easter', 14, (7 * 1) + 2),
    # Ascension Day -> the Friday after Ascension Day
    ('Ascension Day', 0, (7 * 5) + 5),
    # Whitsunday and 7 days after -> the Tuesday after Trinity Sunday
    ('Whitsunday', 7, (7 * 8) + 2),
]

special_transference_rules = {
    'St Philip and St James': lambda cy, ntd: cy.easter + timedelta(days=(7 * 1) + 2) if (cy.easter.month == 4 and cy.easter.day in {22, 24, 25}) else ntd,
    'St Mark': lambda cy, ntd: cy.easter + timedelta(days=(7 * 1) + 4) if (cy.easter.month == 4 and cy.easter.day in {22, 24, 25}) else ntd,
}

### END OF THE TABLES ###

def easter(year):
    # The 'Anonymous Gregorian algorithm'
    # deep magic: no computus, only Easter!!
    Y = year

    a = Y % 19
    b = Y // 100
    c = Y % 100
    d = b // 4
    e = b % 4
    f = (b + 8) // 25
    g = (b - f + 1) // 3
    h = ((19 * a) + b - d - g + 15) % 30
    i = c // 4
    k = c % 4
    l = (32 + (2 * e) + (2 * i) - h - k) % 7
    m = (a + (11 * h) + (22 * l)) // 451

    month = (h + l - (7 * m) + 114) // 31
    day = ((h + l - (7 * m) + 114) % 31) + 1

    return date(year, month, day)

def advent_sunday(year):
    christmas_day = date(year, 12, 25)
    advent_iv = christmas_day - timedelta(days=christmas_day.isoweekday())
    return advent_iv - timedelta(days = 7 * 3)

Day = namedtuple('Day', 'feast days_after transferred other_observances proper_week')

class Year:
    def __init__(self, principal_year=None):
        if principal_year is None:
            principal_year = date.today()

        if isinstance(principal_year, date):
            today = principal_year
            if today >= advent_sunday(today.year):
                principal_year = today.year + 1
            else:
                principal_year = today.year

        self.principal_year = principal_year
        self.__transferred_feasts = set()
    
    @property
    def calendar_years(self):
        return (self.principal_year - 1, self.principal_year)

    @property
    def advent_sunday(self):
        return advent_sunday(self.principal_year - 1)
    @property
    def easter(self):
        return easter(self.principal_year)
    @property
    def last_sunday(self):
        return advent_sunday(self.principal_year) - timedelta(days=7)
    
    @property
    def rcl_year(self):
        return ['A', 'B', 'C'][self.advent_sunday.year % 3]

    @property
    @lru_cache(1)
    def moveable_feasts(self):
        easter = self.easter
        return [(easter + timedelta(days=days_after_easter), name) for name, days_after_easter in moveable_feasts]

    @property
    @lru_cache(1)
    def transference_dates(self):
        moveable_feasts = {k: v for v, k in self.moveable_feasts}
        transference_dates = {}
        for start_day, n_days_after, target_day_count in transference_seasons:
            target_day = self.easter + timedelta(days=target_day_count)
            transference_dates[moveable_feasts[start_day]] = target_day
            for n in range(n_days_after + 1):
                transference_dates[moveable_feasts[start_day] + timedelta(days=n)] = target_day

        return transference_dates

    def first_moveable_feast(self):
        return (moveable_feasts[0][0], self.easter + timedelta(days=moveable_feasts[0][1]))

    def __contains__(self, day):
        if day < self.advent_sunday or day >= advent_sunday(self.principal_year):
            return False
        else:
            return True

    @property
    def transferred_feasts(self):
        return (self.red_letter_days and self.__transferred_feasts)
        
    def dates_within(self, day, month):
        date_in_principal_year = date(self.principal_year, month, day)
        date_in_starting_year = date(self.principal_year - 1, month, day)

        dates = []
        if date_in_starting_year in self: dates.append(date_in_starting_year)
        if date_in_principal_year in self: dates.append(date_in_principal_year)

        return dates

    @property
    @lru_cache(1)
    def red_letter_days(self):
        rlds = []
        for name, date in red_letter_days:
            for rldate in self.dates_within(*date):
                if rldate in self.transference_dates:
                    target = self.transference_dates[rldate]
                    self.__transferred_feasts.add(name)
                    if name in special_transference_rules:
                        rlds.append((name, special_transference_rules[name](self, target)))
                    else:
                        rlds.append((name, target))
                else:
                    rlds.append((name, rldate))

        return sorted(rlds, key=lambda x: x[1])

    @property
    @lru_cache(1)
    def black_letter_days(self):
        blds = {}
        for name, date in black_letter_days:
            for bldate in self.dates_within(*date):
                # a black letter day lapses if it falls on a Sunday or would need to be transferred
                if bldate.weekday() == 6 or bldate in self.transference_dates:
                    continue
                else:
                    blds[bldate] = name
            
        return blds

    def red_letter_day(self, day):
        # todo: replace with bisect? (implies reversing red_letter_days to be (date, name))
        for name, rlday in self.red_letter_days:
            if day == rlday:
                return name

    def black_letter_day(self, day):
        return self.black_letter_days.get(day)
    
    @property
    @lru_cache(1)
    def fixed_feasts(self):
        ff = []

        for name, date in fixed_feasts:
            for ffdate in self.dates_within(*date):
                ff.append((name, ffdate))

        return sorted(ff, key=lambda x: x[1])
    
    def day(self, day=None, include_red_letter=True):
        if day is None: day = date.today()

        if day not in self:
            raise IndexError(f"{day!r} is out of range for the church year {self.calendar_years!r}")

        proper_week = 29 - math.ceil((self.last_sunday - day).days / 7)
    
        other_observances = set()

        _, moveable_feasts_start = self.first_moveable_feast()
        rld = self.red_letter_day(day)
        bld = self.black_letter_day(day)

        if bld: other_observances.add(bld)
        
        if rld and include_red_letter:
            # todo: add the most recent feast to the other observances, or today's moveable feast for non-transferred red letter days
            without_red_letter = self.day(day, include_red_letter=False)
            other_observances = set()
            
            if without_red_letter.days_after == 0:
                other_observances.add(without_red_letter.feast)
                
            return Day(
                feast=rld,
                days_after=0,
                transferred=(rld in self.transferred_feasts),
                other_observances=other_observances,
                proper_week=proper_week
            )
        elif day >= self.last_sunday:
            # Sunday Next before Advent
            return Day(
                feast=last_sunday_name,
                days_after=(day - self.last_sunday).days,
                transferred=False,
                other_observances=other_observances,
                proper_week=proper_week
            )
        elif day >= moveable_feasts_start:
            # Septuagesima to the end of the church year
            idx = bisect.bisect_left(self.moveable_feasts, (day, ''))
            if self.moveable_feasts[idx][0] > day: idx = idx - 1
            
            most_recent_feast_date, name = self.moveable_feasts[idx]
            return Day(
                feast=name,
                days_after=(day - most_recent_feast_date).days,
                transferred=False,
                other_observances=other_observances,
                proper_week=proper_week
            )
        elif day >= self.fixed_feasts[0][1]:
            # Christmastide, Epiphanytide
            for name, feast_date in reversed(self.fixed_feasts):
                if day == feast_date:
                    return Day(feast=name, transferred=False, days_after=0, other_observances=other_observances, proper_week=proper_week)
                elif day > feast_date:
                    days_till_first_sunday_after = ((6 - feast_date.weekday()) or 7)
                    first_sunday_after = feast_date + timedelta(days=days_till_first_sunday_after)
                    if day < first_sunday_after:
                        return Day(
                            feast=name,
                            days_after=(day - feast_date).days,
                            transferred=False,
                            other_observances=other_observances,
                            proper_week=proper_week
                        )
                    else:
                        n_sundays_after = (day - first_sunday_after).days // 7
                        n_weekdays_after = (day - first_sunday_after).days % 7
                        return Day(
                            feast=fixed_feast_sundays[name][n_sundays_after],
                            days_after=n_weekdays_after,
                            transferred=False,
                            other_observances=other_observances,
                            proper_week=proper_week
                        )
                else:
                    continue
        else:
            # Advent
            n_days_into_advents = (day - self.advent_sunday).days
            n_weeks_into_advent = n_days_into_advents // 7
            n_weekdays_after = n_days_into_advents % 7

            return Day(
                feast=advent_sundays[n_weeks_into_advent],
                days_after=n_weekdays_after,
                transferred=False,
                other_observances=other_observances,
                proper_week=proper_week
            )

    def __iter__(self):
        curr_day = self.advent_sunday
        while curr_day < advent_sunday(self.principal_year):
            yield (curr_day, self.day(curr_day))
            curr_day = curr_day + timedelta(days=1)
